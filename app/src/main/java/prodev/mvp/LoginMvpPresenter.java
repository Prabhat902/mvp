package prodev.mvp;

/**
 * Created by ADMIN on 02-09-2018.
 */

public interface LoginMvpPresenter <V extends LoginMvpView> extends MvpPresenter<V> {

    void startLogin(String emailId);

}
