package prodev.mvp;

/**
 * Created by ADMIN on 01-09-2018.
 */

public interface SplashMvpPresenter <V extends SplashMvpView> extends MvpPresenter<V> {

    void decideNextActivity();

}
