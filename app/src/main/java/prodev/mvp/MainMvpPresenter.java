package prodev.mvp;

/**
 * Created by ADMIN on 02-09-2018.
 */

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    String getEmailId();

    void setUserLoggedOut();
}
