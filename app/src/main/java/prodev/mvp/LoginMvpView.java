package prodev.mvp;

/**
 * Created by ADMIN on 01-09-2018.
 */

public interface LoginMvpView extends MvpView {

    void openMainActivity();

    void onLoginButtonClick();
}
