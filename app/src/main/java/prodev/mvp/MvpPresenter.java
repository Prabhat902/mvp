package prodev.mvp;

/**
 * Created by ADMIN on 01-09-2018.
 */

public interface MvpPresenter <V extends MvpView> {

    void onAttach(SplashActivity mvpView);

}
