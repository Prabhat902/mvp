package prodev.mvp;



/**
 * Created by ADMIN on 01-09-2018.
 */

public interface SplashMvpView extends MvpView {
    void openMainActivity();

    void openLoginActivity();
}
